package elements.type.unit;

public class LinkedListNode<T> {
    private T e;
    public LinkedListNode next;

    public LinkedListNode(T e, LinkedListNode next) {
        this.e = e;
        this.next = next;
    }

    public LinkedListNode(T e) {
        this(e, null);
    }

    public LinkedListNode() {
        this(null, null);
    }

    public T getElement() {
        return e;
    }

    @Override
    public String toString() {
        return "[node value: "+ ((null != e) ? e.toString() : "null") +"]"
                + ( (null != next) ? "->" : "" );
    }
}
