package elements.type.unit;

public class Item {
    private String elementName;
    private int elementWeight;

    public Item(String elementName, int elementWeight) {
        this.elementName = elementName;
        this.elementWeight = elementWeight;
    }

    @Override
    public String toString() {
        return "Item{" +
                "elementName='" + elementName + '\'' +
                ", elementWeight=" + elementWeight +
                '}';
    }
}
