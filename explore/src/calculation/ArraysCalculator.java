package calculation;

import elements.type.unit.Item;
import encapsulation.datastructure.impl.generic.SerialArray;

public class ArraysCalculator {
    public static void main(String[] args) {


        // Array Generic Boxing data
        SerialArray<Integer> encapsuleGenericArrays = new SerialArray<>(20);

        for (int i = 0; i < 15; i++) {
            encapsuleGenericArrays.addLast(i);
        }

        System.out.println(encapsuleGenericArrays);
        encapsuleGenericArrays.add(3, 199);
        System.out.println(encapsuleGenericArrays);

        encapsuleGenericArrays.remove(1);
        System.out.println(encapsuleGenericArrays);
        encapsuleGenericArrays.removeElement(9);
        System.out.println(encapsuleGenericArrays);

        // Array Generic Boxing customize
        SerialArray<Item> elementItemEnCapsuleGenericArrays = new SerialArray<>(5);
        for (int j = 0; j < 5; j++) {
            elementItemEnCapsuleGenericArrays.addLast(new Item("Adorable " + String.valueOf(j), j + 1));
        }

        elementItemEnCapsuleGenericArrays.add(1, new Item("Index added", 1));
        System.out.println(elementItemEnCapsuleGenericArrays);
    }
}
