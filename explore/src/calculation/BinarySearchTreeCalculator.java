package calculation;

import encapsulation.datastructure.impl.generic.BinarySearchTree;

import java.util.ArrayList;

public class BinarySearchTreeCalculator {
    public static void main(String[] args) {
        BinarySearchTree<Integer> binarySearchTree = new BinarySearchTree<>();
        binarySearchTree.add(10);
        binarySearchTree.add(23);
        binarySearchTree.add(20);
        binarySearchTree.add(234);
        binarySearchTree.add(50);

        /**
         *        10
         *          \
         *           23
         *         /   \
         *       20    234
         *         \
         *          50
         */

        System.out.println(binarySearchTree.contains(234));

        binarySearchTree.inOrderTraversal();

        binarySearchTree.levelOrder();


        ArrayList<Integer> numberList = new ArrayList<>();
        while (! binarySearchTree.isEmpty()) {
            numberList.add(binarySearchTree.removeMin());
        }

        System.out.println(numberList);
        System.out.println(binarySearchTree.getSize());
    }
}
