package calculation;

import encapsulation.datastructure.impl.generic.Stack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StackCalculator {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < 5; i++) {
            stack.push(i);
            System.out.println(stack);
        }

        stack.pop();
        System.out.println(stack);


        System.out.println(new StackCalculator().isMatchedQuote("{[][()}"));
    }

    public boolean isMatchedQuote(String quoteSerial) {
        List<Character> matchedQuotes = new ArrayList<Character>(Arrays.asList('{', '[', '('));
        Stack<Character> characterEnCapsuleStack = new Stack<>();
        for (int i = 0; i < quoteSerial.length(); i++) {
            char matchedCharacter = quoteSerial.charAt(i);

            if (matchedQuotes.contains(Character.valueOf(matchedCharacter))) {
                characterEnCapsuleStack.push(matchedCharacter);
            } else {
                if (characterEnCapsuleStack.isEmpty()) {
                    return false;
                }

                char topChar = characterEnCapsuleStack.pop();
                if (matchedCharacter == '}' && topChar != '{') {
                    return false;
                }

                if (matchedCharacter == ']' && topChar != '[') {
                    return false;
                }

                if (matchedCharacter == ')' && topChar != '(') {
                    return false;
                }
            }
        }

        return characterEnCapsuleStack.isEmpty();
    }
}
