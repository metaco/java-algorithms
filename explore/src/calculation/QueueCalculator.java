package calculation;

import encapsulation.datastructure.model.CapusleQueue;
import encapsulation.datastructure.impl.generic.CircularQueue;
import encapsulation.datastructure.impl.generic.Queue;

import java.util.Random;

public class QueueCalculator {
    public static void main(String[] args) {
        Queue<Integer> enCapsuleQueue = new Queue<>(5);

        for (int i = 0; i < 10; i++) {
            enCapsuleQueue.enQueue(i);
            System.out.println(enCapsuleQueue);
            if (i % 3 == 2) {
                System.out.println("i = " + i + " \n... " + i % 3);
                enCapsuleQueue.deQueue();
                System.out.println(enCapsuleQueue);
            }
        }

        CircularQueue<Integer> circularQueue = new CircularQueue<>(5);
        for (int i = 0; i < 5; i++) {
            circularQueue.enQueue(i);
            System.out.println(circularQueue);

            if (i % 3 == 2) {
                //;
                circularQueue.deQueue();
                System.out.println(circularQueue);
            }
        }

        circularQueue.enQueue(10);
        System.out.println(circularQueue);
        circularQueue.enQueue(12);
        System.out.println(circularQueue);
        circularQueue.enQueue(15);
        System.out.println(circularQueue);


        int opCount = 100000;
        // ArrayQueue
        // CircularQueue
        Queue<Integer> capsuleQueue = new Queue<>(5);
        CircularQueue<Integer> capsuleCircularQueue = new CircularQueue<>(5);

        System.out.println("ArrayQueue time: " + testQueue(capsuleQueue, opCount) + "s");
        System.out.println("CircularQueue time : " + testQueue(capsuleCircularQueue, opCount) + "s");
    }

    private static double testQueue(CapusleQueue<Integer> queue, int opCount) {
        long startTime = System.nanoTime();
        Random random = new Random();
        for (int i = 0; i < opCount; i++) {
            queue.enQueue(random.nextInt());
        }

        for (int i = 0; i < opCount; i++) {
            queue.deQueue();
        }

        long endTime = System.nanoTime();
        return (endTime - startTime ) / 1000000000.0;
    }
}
