package calculation;

import encapsulation.datastructure.impl.generic.LinkedList;

public class LinkedListCalculator {
    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>();
        for (int i = 0; i < 4; i++) {
            linkedList.addElement(String.valueOf(i));
        }

        linkedList.addElement("Srt");
        linkedList.addElement("Srt");
        linkedList.addElementAfter(String.valueOf("2"), String.valueOf("2"));

        linkedList.addElement("Sr");
        linkedList.traversal();

        System.out.println("-----\n");
        // linkedList.removeElement("Srt");
        linkedList.removeRepeatableElements();

        linkedList.traversal();
    }
}
