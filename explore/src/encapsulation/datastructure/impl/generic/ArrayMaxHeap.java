package encapsulation.datastructure.impl.generic;

import encapsulation.datastructure.model.CapsuleHeap;

/**
 *  数组中的元素第一个位置不用，其他位置向后挪动一位
 *
 * @param <E>
 */
public class ArrayMaxHeap<E extends Comparable<E>> implements CapsuleHeap<E> {
    private SerialArray<E> data;

    public ArrayMaxHeap(int capacity) {
        this.data = new SerialArray<>(capacity);
    }

    public ArrayMaxHeap() {
        this.data = new SerialArray<>();
    }

    // 整理一个数组成为一个最大堆 , 替换起始位置从 最后一个非叶子节点开始
    // （所有非叶子节点进行 siftDown 操作）
    // 也就是最后一个叶子节点的父级元素节点
    public ArrayMaxHeap(E[] arrayHeap) {
        data = new SerialArray<>(arrayHeap);

        for (int i = parentIndex(arrayHeap.length - 1); i >= 0 ; i-- ) {
            siftDown(i);
        }
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public int getSize() {
        return data.getSize();
    }

    /**
     * 检查索引关系
     * @param index int (检查父级节点的元素关系）
     * @return int
     */
    private int parentIndex(int index) {
        return index == 0 ? -1 : (index - 1) / 2;
    }

    private int leftChildIndex(int index) {
        return index * 2 + 1;
    }

    private int rightChildIndex(int index) {
        return index * 2 + 2;
    }

    // 向堆中添加元素
    public void add(E e) {
        data.addLast(e);
        // 维护堆的性质
        // sift up (希望上浮元素加入的索引是多少）
        siftUp(data.getSize() - 1);
    }

    private void siftUp(int index) {
        // 上浮的元素必须不是根节点（也就是索引位置必须比根节点大）
        while (index > 0 && (data.get(parentIndex(index)).compareTo(data.get(index)) < 0)) {
            // 比较当前元素和其父亲元素的关系, （如果父级元素比当前元素小，执行上浮操作
            // 上浮操作也是 ，位置交换
            data.swap(index, parentIndex(index));
            index = parentIndex(index);
        }
    }

    public E heapTop() {
        if (0 == data.getSize())
            return null;
        return data.get(0);
    }

    // 堆顶元素，为最大元素
    public E extract() {
        E result = heapTop();
        // 1. 交换两个位置的元素
        data.swap(0, data.getSize() - 1);
        // 组合实现Ω    堆顶元素
        data.removeLast(); // 元素已经迁移

        // 此时最大堆的性质已经被删除， 需要 sift down
        siftDown(0);
        return result;
    }

    private void siftDown(int index) {
        // 第一个子节点的索引，已经越界则不能跳出
        while (leftChildIndex(index) < data.getSize()) {
            // 找出左右孩子的最大的节点
            int leftChildIndex = leftChildIndex(index);
            // 如果右子节点的（leftChildIndex + 1 为右边索引）位置存在，而且右子节点的索引比左孩子大
            if (leftChildIndex + 1 < data.getSize() && data.get(leftChildIndex + 1)
                    .compareTo(data.get(leftChildIndex)) > 0) {
                leftChildIndex = rightChildIndex(index);
            }
            // 当前节点比即将交换的位置元素大， 无需交换，siftDown 完成
            if (data.get(index).compareTo(data.get(leftChildIndex)) >= 0) {
                break;
            }

            data.swap(index, leftChildIndex);
            index = leftChildIndex;
        }
    }

    // 取出最大的元素，并且替换堆顶元素为 e

    /**
     * 取出最大的堆顶元素，然后替换堆顶元素, 使用siftDown 重新构建最大堆
     * @param e E 替换元素
     * @return E
     */
    public E replace(E e) {
        E result = heapTop();
        //
        data.set(0, e);
        siftDown(0);
        return result;
    }

    // 整理一个数组成为一个最大堆 , 替换起始位置从 最后一个非叶子节点开始
    // （所有非叶子节点进行 siftDown 操作）
    // 也就是最后一个叶子节点的父级元素节点
    public void heapify() {

    }

    @Override
    public String toString() {
        return "ArrayMaxHeap{" +
                "data=" + data +
                '}';
    }
}
