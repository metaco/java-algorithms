package encapsulation.datastructure.impl.generic;

public class SerialArray<E> {

    private static final int REALLOCATE_SCALAR = 2;

    private E[] data;
    private int size;
    /**
     *
     * @param capacity
     */
    public SerialArray(int capacity) {
        // Java 语法的局限性
        data = (E[]) new Object[capacity];
        size = 0;
    }

    public SerialArray() {
        this(10);
        size = 0;
    }

    public SerialArray(E[] data) {
        this.data = (E[]) new Object[data.length];
        for (int i = 0; i < data.length; i++) {
            this.data[i] = data[i];
        }
        size = data.length;
    }

    public int getSize() {
        return size;
    }

    public int getCapacity() {
        return data.length;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index is illegal");
        }
        return data[index];
    }

    public E getFirst() {
        return get(0);
    }

    public E getLast() {
        return get(size - 1);
    }

    public void set(int index, E e) {
        if (index < 0 || index < size) {
            throw new IllegalArgumentException("Index is illegal");
        }
        data[index] = e;
    }

    public void addLast(E e) {
        add(size, e);
    }

    public void addFirst(E e) {
        add(0, e);
    }

    public void add(int index, E e) {
        if (index < 0 || index > size ) {
            throw new IllegalArgumentException("encapsule-Array is full, OR require index >= 0 and index <= size");
        }
        if (size == data.length  ) {
            resize(REALLOCATE_SCALAR * data.length);
        }
        // 从后向前挪动
        for (int i = size - 1; i >= index; i--) {
            data[i + 1] = data[i];
        }

        data[index] = e;
        size++;
    }

    public int find(E e) {
        for (int index = 0; index < size; index++) {
            if (data[index].equals(e)) {
                return index;
            }
        }
        return -1;
    }

    /**
     * remove the index of elements
     * @param index
     * @return
     */
    public E remove(int index) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("index is illegal");
        }

        E resultElement = data[index];
        for (int internalIndex = index + 1; internalIndex < size; internalIndex++) {
            data[internalIndex - 1] = data[internalIndex];
        }
        size--;
        data[size] = null;

        // 动态缩小 , 所存储的元素已经是容积的1/4 时，缩小容器为原来的一半,(避免发生算法震荡
        if (size == data.length / 4 && 0 != data.length / 2) {
            resize( data.length / 2);
        }
        return resultElement;
    }

    public E removeFirst() {
        return remove(0);
    }

    public E removeLast() {
        return remove(size -1);
    }

    /**
     * 用户知道传递的元素是什么类型， 但是无法重复删除元素（只能删除一个元素
     * @param e
     */
    public void removeElement(E e) {
        int index = find(e);
        if (index != -1) {
            remove(index);
        }
    }

    private void resize(int newCapacity) {
        E[] newData = (E[]) new Object[newCapacity];
        for (int i = 0; i < size ; i++) {
            newData[i] = data[i];
        }
        // 指针赋值
        data =  newData;
    }

    public void swap(int index1, int index2) {
        if (index1 < 0 || index1 > size || index2 < 0 || index2 >size) {
            throw new IllegalArgumentException("index is illegal.");
        }

        E d = data[index1];
        data[index1] = data[index2];
        data[index2] = d;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("EnCapsuleGeneric array size = %d, capacity = %d\n", size, data.length));
        stringBuilder.append("[");

        for (int index = 0;  index < size; index++) {
            stringBuilder.append(data[index]).append(index != size - 1 ? "," : "");
        }

        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
