package encapsulation.datastructure.impl.generic;

import elements.type.unit.LinkedListNode;

/** 带有头结点，尾节点的链表，一般插入元素从队尾插入，删除元素从对头删除
 * @param <E>
 */
public class LinkedList<E> {
    private LinkedListNode<E> head, tail;
    private int size;

    public LinkedList() {
        this.head = new LinkedListNode<>();
        this.tail = new LinkedListNode<>();
        size = 0;
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return 0 == size;
    }

    private LinkedListNode findElement(E e) {
        if (isEmpty())  {
            return null;
        }
        LinkedListNode currentNode =  head.next;
        while (null != currentNode.next) {
            if (currentNode.getElement().equals(e)) {
                return currentNode;
            }
            currentNode = currentNode.next;
        }

        return null;
    }

    public void addElementAfter(E afterElement, E insertAbleElement) {
        LinkedListNode node = findElement(afterElement);
        if (null != node) {
            LinkedListNode<E> insertElement = new LinkedListNode<>(insertAbleElement);
            insertElement.next = node.next;
            node.next = insertElement;
            size++;
        }
    }

    public void addElement(E e) {
        LinkedListNode<E> elementNode = new LinkedListNode<>(e);
        if (null == head.next) {
            head.next = elementNode;
            tail = elementNode;
        }else {
            LinkedListNode lastNode = tail;
            lastNode.next = elementNode;
            tail = elementNode;
        }
        size++;
    }

    public void removeElement(E e) {
        if (isEmpty())  {
            return ;
        }

        LinkedListNode previousNode = head;
        while (null != previousNode.next) {
            if (previousNode.next.getElement().equals(e)) {
                LinkedListNode delNode = previousNode.next;
                // 置换链表的关联关系到下一号元素
                previousNode.next = delNode.next;
                // delNode.next = null; // 置空删除元素的关联关系
                // ?(Java 中的一个疑惑？如果一个对象的成员对象被另一个对象赋值上去，该赋值对象置位为空，则被赋值的对象还有数据？
                size--;
            }else {
                previousNode = previousNode.next;
            }
        }
    }

    public void removeRepeatableElements() {
        if (getSize() <= 1) {
            return ;
        }

        // 从一号节点开始对比每个元素的值，后续节点如果存在和标志位元素一致的元素，删除该元素
        // 1. 从外层看就是元素的循环遍历
        LinkedListNode currentNode = head.next;
        while (null != currentNode.next) {
            // 2 . 删除和currentNode 相同的元素
            LinkedListNode scannerPreviousNode = currentNode;
            while (null != scannerPreviousNode.next) {
                if (scannerPreviousNode.next.getElement().equals(currentNode.getElement())) {
                    LinkedListNode delNode = scannerPreviousNode.next;
                    // 置换链表的关联关系到下一号元素
                    scannerPreviousNode.next = delNode.next;
                    // delNode.next = null; /// 置空删除元素的关联关系
                    size--;
                }else {
                    scannerPreviousNode = scannerPreviousNode.next;
                }
            }

            if (null == currentNode.next) {
                break;
            }
            currentNode = currentNode.next;
        }
    }

    public void traversal() {
        if (isEmpty()) {
            return ;
        }
        LinkedListNode currentNode = head.next;
        while (null != currentNode) {
            System.out.println(currentNode);
            currentNode = currentNode.next;
        }
    }

}
