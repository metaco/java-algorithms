package encapsulation.datastructure.impl.generic;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree<E extends Comparable<E>> {

    /**
     *  1. 二分搜索树的每个节点都左子节点 (左子树)大，比右子节点（右子树）小
     *  2. 每颗子树也是一颗二分搜索树
     *  3. (目前二分搜索树不包含重复元素）
     */

    private BinarySearchNode root;

    private int size;

    public BinarySearchTree() {
        root = null;
        size = 0;
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return 0 == size;
    }

    public void add(E e) {
        root = addBinarySearchNode(root, e);
    }

    private BinarySearchNode addBinarySearchNode(BinarySearchNode node , E e) {
        // 终止条件
        if (null == node) {
            size++;
            return new BinarySearchNode(e);
        }
        if (e.compareTo(node.e) < 0 ) {
            // 根节点左子树为空
            node.left = addBinarySearchNode(node.left, e);
        } else if (e.compareTo(node.e) > 0) {
            // 根节点右子树为空
            node.right = addBinarySearchNode(node.right, e);
        }

        return node;
    }

    /**
     * 查看二分搜索树是否包含元素 E
     * @param e
     * @return
     */
    public boolean contains(E e) {
        return containNode(root , e);
    }

    private boolean containNode(BinarySearchNode node , E e) {
        if (null == node) {
            return false;
        }

        if (e.compareTo(node.e) == 0) {
            return true;
        }else if (e.compareTo(node.e) < 0) {
            return containNode(node.left, e );
        } else if (e.compareTo(node.e) > 0) {
            return containNode(node.right, e);
        }

        return false;
    }

    /**
     *  二分搜索树的层序遍历
     */
    public void levelOrder() {
        Queue<BinarySearchNode> queue = new LinkedList<>();
        queue.add(root);

        while ( ! queue.isEmpty()) {
            BinarySearchNode currentNode = queue.remove();
            System.out.println(currentNode.e);
            if (null != currentNode.left) {
                queue.add(currentNode.left);
            }

            if (null != currentNode.right) {
                queue.add(currentNode.right);
            }
        }
    }

    // 删除一棵二分搜索树的最小值，最大值
    // 条件1：
    //     如果该树的最大值，最小值在叶子节点，则可以直接删除
    // 条件2:
    //     如果该树的最小值，最大值不在叶子节点，则需要找到
    public E getMinimum() {
        if (0 == size) {
            return null;
        }

        return getMinimum(root).e;
    }

    private BinarySearchNode getMinimum(BinarySearchNode node) {
        if (null == node.left) {
            return node;
        }
        // 递归遍历左子树
        return getMinimum(node.left);
    }

    public E getMaximum() {
        if (0 == size) {
            return null;
        }

        return getMinimum(root).e;
    }

    private BinarySearchNode getMaximum(BinarySearchNode node) {
        if (null == node.right) {
            return node;
        }
        // 递归遍历左子树
        return getMinimum(node.right);
    }

    /**
     *
     * @return E
     */
    public E removeMin() {
        E result = getMinimum();
        // 删除的元素可能不在左子树叶子节点
        root = removeMin(root);
        // 删除最小值后的 root
        return result;
    }

    /**
     *
     * @param node BinarySearchNode
     * @return BinarySearchNode
     */
    private BinarySearchNode removeMin(BinarySearchNode node) {
        // 如果node.left 为空，无法再递归
        if (null == node.left) {
            // 但是如果当前节点可能有右子树
            BinarySearchNode removeNode = node.right;
            // 清空 被删除
            node.right = null;

            size--;
            return removeNode;
        }

        node.left = removeMin(node.left);
        return node;
    }


    public E removeMax() {
        // 删除的元素可能不在右子树叶子节点
        E result = getMaximum();
        root = removeMax(root);
        return result;
    }

    /**
     *
     * @param node BinarySearchNode
     * @return BinarySearchNode
     */
    private BinarySearchNode removeMax(BinarySearchNode node) {
        if (null == node.right) {
            BinarySearchNode removeNode = node.left;
            node.left = null;

            size --;
            return removeNode;
        }

        node.right = removeMax(node.right);
        return node;
    }


    public void inOrderTraversal() {
        inOrderTraversal(root);
    }

    // 一棵二分搜索树中序遍历即就是 一棵树的小至大顺序
    private void inOrderTraversal(BinarySearchNode node) {
        if (null == node) {
            return ;
        }
        inOrderTraversal(node.left);
        System.out.println(node.e);
        inOrderTraversal(node.right);
    }


    private class BinarySearchNode {
        public E e;
        public BinarySearchNode left, right;

        public BinarySearchNode(E e) {
            this.e = e;
            left = null;
            right = null;
        }
    }
}
