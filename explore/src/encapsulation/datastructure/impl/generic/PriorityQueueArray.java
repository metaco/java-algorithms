package encapsulation.datastructure.impl.generic;

import encapsulation.datastructure.model.CapusleQueue;

public class PriorityQueueArray <E extends Comparable<E>> implements CapusleQueue<E> {
    private ArrayMaxHeap<E> maxHeap;

    public PriorityQueueArray() {
        this.maxHeap = new ArrayMaxHeap<>();
    }

    @Override
    public int getSize() {
        return maxHeap.getSize();
    }

    @Override
    public boolean isEmpty() {
        return maxHeap.isEmpty();
    }

    @Override
    public void enQueue(E e) {
        maxHeap.add(e);
    }

    @Override
    public E deQueue() {
        return maxHeap.extract();
    }

    @Override
    public E getFront() {
        return maxHeap.heapTop();
    }
}
