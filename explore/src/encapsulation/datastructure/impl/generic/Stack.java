package encapsulation.datastructure.impl.generic;

import encapsulation.datastructure.model.CapsuleStack;

public class Stack<E> implements CapsuleStack<E> {

    private final SerialArray<E> capsuleArray;

    public Stack() {
        this.capsuleArray = new SerialArray<>();
    }

    @Override
    public int getSize() {
        return capsuleArray.getSize();
    }

    @Override
    public boolean isEmpty() {
        return capsuleArray.isEmpty();
    }

    @Override
    public void push(E e) {
        capsuleArray.addLast(e);
    }

    @Override
    public E pop() {
        return capsuleArray.removeLast();
    }

    @Override
    public E peek() {
        return capsuleArray.getLast();
    }

    @Override
    public String toString() {
        return "Stack : " + capsuleArray.toString() ;
    }
}
