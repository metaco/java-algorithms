package encapsulation.datastructure.impl.generic;

import encapsulation.datastructure.model.CapusleQueue;

public class Queue<E> implements CapusleQueue<E> {
    private final SerialArray<E> capsuleGenericArrays;

    public Queue(int capacity) {
        this.capsuleGenericArrays = new SerialArray<>(capacity);
    }

    @Override
    public int getSize() {
        return capsuleGenericArrays.getSize();
    }

    @Override
    public boolean isEmpty() {
        return capsuleGenericArrays.isEmpty();
    }

    @Override
    public void enQueue(E e) {
        capsuleGenericArrays.addLast(e);
    }

    @Override
    public E deQueue() {
        return capsuleGenericArrays.removeFirst();
    }

    @Override
    public E getFront() {
        return capsuleGenericArrays.getFirst();
    }

    @Override
    public String toString() {
        return "Queue " + capsuleGenericArrays.toString();
    }
}
