package encapsulation.datastructure.impl.generic;

import encapsulation.datastructure.model.CapusleQueue;

import java.util.Arrays;

public class CircularQueue<E> implements CapusleQueue<E> {

    private static final int CIRCULAR_QUEUE_REALLOCATE_SCALAR  = 2;
    private E[] data;

    private int front;
    private int tail;

    private int size;

    public CircularQueue(int capacity) {
        data = (E[]) new Object[capacity + 1];
        front = 0;
        tail = 0;
        size = 0;
    }

    public CircularQueue() {
        this(10);
    }

    public int getCapacity() {
        return data.length - 1;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return front == tail;
    }

    @Override
    public void enQueue(E e) {
        // 队列是否为满.. (tail 的下一个位置是否和 front 所指向的位置相同，
        // 由于循环队列的特殊性，tail 所指向的位置既可以是正向位置，又可以是反向位置，
        // 所以预留一个位置用于tail 循环到反向位置判断队列front 位置是否相等
        if ((tail + 1) % data.length == front) {
            // 重新分配存储空间
            resize(getCapacity() * CIRCULAR_QUEUE_REALLOCATE_SCALAR);
        }

        data[tail] = e;
        tail = (tail + 1) % data.length;
        size ++;
    }

    @Override
    public E deQueue() {
        if (! isEmpty()) {
            E resultElement = data[front];
            data[front] = null;
            // 维护对头指针
            front = (front + 1 ) % data.length;
            size--;
            if (size  == getCapacity() / 4 && 0 != getCapacity() / 2) {
                resize( getCapacity() / 2);
            }
            return resultElement;
        }
        throw new RuntimeException("cannot dequeue from an empty queue.");
    }

    @Override
    public E getFront() {
        if (! isEmpty()) {
            return data[front];
        }

        throw new RuntimeException("cannot dequeue from an empty queue.");
    }

    private void resize(int newCapacity) {
        E[] newData = (E[]) new Object[newCapacity + 1];
        // 循环队列中的所有元素拷贝当前位置
        for (int i = 0; i < size; i++) {
            newData[i] = data[ (i + front) % data.length];
        }

        data = newData;
        front = 0;
        tail = size;
    }

    @Override
    public String toString() {
        return "CircularQueue{" +
                "data= " + Arrays.toString(data) +
                ", front=" + front +
                ", tail=" + tail +
                ", size=" + size +
                '}';
    }
}
