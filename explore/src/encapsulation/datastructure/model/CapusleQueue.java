package encapsulation.datastructure.model;

public interface CapusleQueue<E> {
    int getSize();
    boolean isEmpty();

    void enQueue(E e);
    E deQueue();

    E getFront();
}
