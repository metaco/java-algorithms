package encapsulation.datastructure.model;

public interface CapsuleHeap<E> {
    boolean isEmpty();
    int getSize();


}
